---
layout: default
---

## Quickstart Guide

Click [here](https://bitbucket.org/UBCCS/jekyll-faculty/src/master/) to return to the 
repository website that shows instructions on how to use
and customize this UBC CS faculty and course static website generator yourself.  Browse
the markdown files in particular (```.md``` file extension; start with ```index.md``` -- make sure you view the raw source).

Browse the remainder of this demonstration website to gain a sense of what can
be easily and efficiently generated and maintained (like $x^2 + 1 = 17$, ```while(true) ++i```, and the references below).

---------------
## About Me

<img class="profile-picture" src="headshot.jpg">

I am an professor of teaching of computer science at the University of British Columbia.


## Research Interests

The intersection of technology and learning: methods to enhance learning through technology, use of technology to understand phenomena of learning, and pedagogy of computer science. I co-developed and extended the Classroom Presenter system for (shockingly) classroom presentations, modifying the system to increase instructors' flexibility in presentation and students' engagement in and impact on presentations and using the system as a tool to understand how and why interaction happens in the classroom. More recently, I have been developing a framework to support open-ended, creative assignments in the first two Computer Science courses in collaboration with Chris Head, an undergraduate research assistant. Preliminary experiments with the framework suggest that these assignments are engaging to students and that they may be especially engaging for female and undecided students.

I am also interested in human-computer interaction (HCI) and artificial intelligence (AI). My previous work has spanned core AI research in planning, combining Boolean satisfiability solving and Linear Programming to efficiently model and solve resource-constrained problems; AI/HCI work in programming by demonstration (PBD), using machine learning techniques to negotiate interface choices with users in a PBD editing system; and the HCI/education work described above. For example, I worked with Erica Huang, an undergraduate research assistant, to apply local search techniques to developing improved cell phone keypad layouts for users of Zhu-Yin (i.e., Taiwanese users).

Purely in terms of pedagogical research, I enjoy developing engaging and effective pedagogical techniques for challenging teaching venues: large classes, distance classes, and esoteric subjects. My prior work has included techniques for taking advantage of the size of large classes and kinesthetic (physical) activities for teaching various computer science topics.

## Selected Publications

{% bibliography --file selected.bib %}
